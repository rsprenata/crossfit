<!DOCTYPE html>
<html lang="en">
<head>
    <link href="https://fonts.googleapis.com/css?family=Asap:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="fonts/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="icons.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Crossfit</title>
</head>
    <body>
        <header class="header">
            <div class="container">
                <h1 class="header-logo">
                    <img src="img/logo.svg" alt="Logo Tecnofit">
                </h1>
                <form class="form-email">
                    <h1 class="title">
                        CrossFit de alta </br>
                        performance</br>
                        sem um pingo de suor.
                    </h1>
                    <p class="text">
                        Tecnofit é o software de gerenciamento empresarial criado
                        para levar os seus ganhos ao limite! Você está preparado?
                    </p>
                    <div class="field -space">
                        <div class="field-input">
                            <input type="email" id="email" class="field-email" required/>
                            <label for="email" class="field-label">Seu endereço de e-mail</label>
                        </div>
                        <button class="field-button -fyellow text -blue pointer">
                            Teste grátis
                        </button>
                    </div>
                    <p class="text -small">
                        Experimente o Tecnofit gratuitamente por 15 dias.
                    </p>
                </form>
                <div class="macbook">
                </div>
            </div>
        </header>
        <section class="sub-header">
            <div class="container -subheader">
                <h1 class="title -large -blue">
                    Tecnofit é controle </br>
                    do começo ao fim!
                </h1>
                <div class="text -rule -large -grey">
                        A Tecnofit está há dez anos no mercado de gestão de empresas
                        para o segmento Health & Fitness - como academias, estúdios de
                        pilates, escolas de natação, entre outros.
                    <p>
                        Para tornar nosso produto ainda mais completo, desenvolvemos
                        um sistema específico para CrossFit para atender as
                        particularidades desse segmento.
                    </p>
                    <p>
                        Feito por uma equipe apaixonada pela modalidade, o resultado
                        foi um software eficiente, que entende exatamente quais são as
                        reais necessidades de um box!
                    </p>
                </div>
            </div>
        </section>
        <section class="funcionalidades">
            <div class="container">
                <div class="performance">
                    <div class="performance-title">
                        <h1 class="title -large -blue -fancy">
                            Uma rotina de
                            alta performance
                        </h1>
                    </div>
                    <div class="performance-text">
                        <p class="text -large -grey ">Confira as principais funcionalidades do Tecnofit para CrossFit.</p>
                    </div>
                </div>

                <div class="grid-performance">
                    <?php for ($i = 0; $i < 6; $i++) { ?>
                        <div class="grid-performance-item">
                            <section class="card-performance">
                                <div class="card-performance-img">
                                    <img src="./img/icone.png" alt="Recorrência de Pagamento">
                                </div>
                                <div class="card-performance-content">
                                    <h2 class="title -blue -small funcionalidade-title">
                                        Recorrência de Pagamento
                                    </h2>
                                    <p class="text -grey -medium funcionalidade-text">
                                        O Tecnofit permite pagamento recorrente por cartão de crédito ou boleto bancário. Assim, seu aluno não se preocupa em pagar e você não precisa cobrá-lo.
                                    </p>
                                </div>
                            </section>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <section class="tecnofit-box-color">
            <div class="tecnofit-box-back">
                <div class="container">
                    <div class="box-header">
                        <div class="box-header-logo">
                            <img src="./img/logobox.svg" alt="">
                        </div>
                        <div class="box-header-content">
                            <h3 class="title -large2 ">
                                Contratando o sistema Tecnofit, você
                                também pode oferecer o aplicativo
                                Tecnofit Box aos seus alunos.
                                E, o melhor: sem custo adicional!
                            </h3>
                            <p class="text -large">Conheça suas principais funcionalidades:</p>
                        </div>
                    </div>
                    <ul class="grid-funcionalidades-box">
                        <li class="grid-funcionalidades-item">
                            <div class="text cx">
                                <div class="media-icon">
                                    <span class="icon-clock"></span>
                                </div>
                                <div>Cronômetro para contagem do tempo e repetições; </div>
                            </div>
                        </li>
                        <li class="grid-funcionalidades-item">
                            <div class="text cx">
                                <div class="media-icon">
                                    <span class="icon-wave"></span>
                                </div>
                                <div>Controle de lesões;  </div>
                            </div>
                        </li>
                        <li class="grid-funcionalidades-item">
                            <div class="text cx">
                                <div class="media-icon">
                                    <span class="icon-edit"></span>
                                </div>
                                <div>Conferir e comentar as notícias do box; </div>
                            </div>
                        </li>
                        <li class="grid-funcionalidades-item">
                            <div class="text cx">
                                <div class="media-icon">
                                    <span class="icon-graph"></span>
                                </div>
                                <div>Estatísticas de movimentos e evolução de desempenho dos WODs; </div>
                            </div>
                        </li>
                        <li class="grid-funcionalidades-item">
                            <div class="text cx">
                                <div class="media-icon">
                                    <span class="icon-play"></span>
                                </div>
                                <div>Opção de visualização dos movimentos com vídeo;  </div>
                            </div>
                        </li>
                        <li class="grid-funcionalidades-item">
                            <div class="text cx">
                                <div class="media-icon">
                                    <span class="icon-flag"></span>
                                </div>
                                <div>Integração com Gympass;  </div>
                            </div>
                        </li>
                        <li class="grid-funcionalidades-item">
                            <div class="text cx">
                                <div class="media-icon">
                                    <span class="icon-pin"></span>
                                </div>
                                <div>Check-in para garantir presença em aulas; </div>
                            </div>
                        </li>
                        <li class="grid-funcionalidades-item">
                            <div class="text cx">
                                <div class="media-icon">
                                    <span class="icon-card"></span>
                                </div>
                                <div>Possibilidade de realizar pagamentos e acompanhar histórico de transações financeiras; </div>
                                <div class="funcionalidade-exclusiva">
                                    <img src="./img/funcionalidade-exclusiva.png" alt="Funcionalidade exclusiva">
                                </div>
                            </div>
                        </li>
                        <li class="grid-funcionalidades-item">
                            <div class="text cx">
                                <div class="media-icon">
                                    <span class="icon-flag"></span>
                                </div>
                                <div>Registro de recordes pessoais;  </div>
                            </div>
                        </li>
                    </ul>
                    <div class="card-info">
                        <div class="card-item">
                            <div class="card-item-box">
                                <div class="media-icon-card">
                                    <span class="icon-trophy"></span>
                                </div>
                                <div class="text"><span class="text -yellow -bold">Leaderboard para campeonatos:</span> funcionalidade gratuita
                                    que possibilita o box promover campeonatos internos, baterias de
                                    exercícios, criar equipes e muito mais!
                                </div>
                            </div>
                            <div class="funcionalidade-exclusiva">
                                <img src="./img/gratis.png" alt="Funcionalidade exclusiva">
                            </div>
                        </div>
                        <div class="card-item">
                            <div class="card-item-box">
                                <div class="media-icon-card">
                                    <span class="icon-star"></span>
                                </div>
                                <div class="text"><span class="text -yellow -bold">Nível Fitness:</span> pontuação referente aos resultados
                                    dos últimos 90 dias de treino do atleta, que pode ser usada para
                                    gerar motivação e competições entre os CROSSFITTERS.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="smartphone">
                <button class="btn btn-left">
                    <div class="seta">
                        <span class="icon-seta-left text -yellow"></span>
                    </div>
                </button>
                <img src="./img/1.gif" alt=""class="imagem">
                <button class="btn btn-right">
                    <div class="seta">
                        <span class="icon-seta-right text -yellow"></span>
                    </div>
                </button>
            </div>
        </section>
        <section class="planos-precos">
           <div class="container">
               <div class="title-box">
                   <div class="title -blue -large -space">
                    Planos e Preços
                   </div>
                   <ul class="text -yellow -subtitle">
                       <li> <span class="text -blue">Sem taxas de implantação ou fidelidade</span></li>
                       <li> <span class="text -blue">Em média custa menos de <span class="text -bold -blue">R$1</span> ao mês por aluno</span></li>
                   </ul>
               </div>
               <div class="precos">
                   <?php for ($i = 0; $i < 5; $i++) { ?>
                       <div class="preco-box">
                           <p class="text -blue">Até <span class="text -bold -blue">50</span> alunos</p>
                           <span class="title -little -blue">R$</span>
                           <span class="title -blue">89</span>
                           <p class="text -medium -grey">(mensal)</p>
                       </div>
                   <?php }?>
               </div>
           </div>
        </section>
        <section class="prefooter-color">
            <div class="prefooter">
                <div class="container">
                    <div class="prefooter-content">
                        <div class="text -blue -space">Gostou do que viu até aqui? Essa é apenas uma amostra do que o <span class="text -blue -bold">Tecnofit</span>
                            é capaz de fazer pelo seu box de CrossFit. Afinal de contas, sabemos que você
                            precisa de tempo para aquilo que é importante: o esporte e os seus alunos!
                        </div>
                        <div class="title -blue -medium">Preencha o formulário e comece o seu teste gratuito de 15 dias!</div>
                        <div class="field -space2">
                            <div class="field-input">
                                <input type="email" id="email2" class="field-email" required/>
                                <label for="email2" class="field-label">Seu endereço de e-mail</label>
                            </div>
                            <button class="field-button -fblue text -bold pointer">
                                Teste grátis
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <footer class="footer">
            <div class="container">
                <div class="footer-content">
                    <div class="social-network">
                        <a href="https://pt-br.facebook.com/"><div class="icon-facebook"> </div></a>
                        <a href="https://pt-br.facebook.com/"><div class="icon-instagram"> </div></a>
                        <a href="https://pt-br.facebook.com/"><div class="icon-youtube"> </div></a>
                    </div>
                    <div class="contact">
                        <div class="text cx-contato">
                            <div class="media-icon-footer">
                                <span class="icon-telefone"></span>
                            </div>
                            <div>(41) 3086.2366</div>
                        </div>
                        <div class="text cx-contato">
                            <div class="media-icon-footer">
                                <span class="icon-mensagem"></span>
                            </div>
                            <div>contato@tecnofit.com.br</div>
                        </div>
                    </div>
                    <h4 class="footer-logo">
                        <img src="img/logo.svg" alt="Logo Tecnofit">
                    </h4>
                </div>
            </div>
        </footer>

        <script type="text/javascript" src="script.js"></script>

    </body>
</html>

