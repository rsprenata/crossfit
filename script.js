// declarar 4 variáveis (array das imagens, botoes, image principal)
// document.querySelector
// addEventListener
// manipular atributos

// navegação com as teclas (esquerda, direita)

var activeImage = 0

var images = [
    'img/1.gif',
    'img/2.gif',
    'img/3.gif',
    'img/2.gif',
]

var buttonPrevious = document.querySelector('.btn-left')
var buttonNext = document.querySelector('.btn-right')
var mainImage = document.querySelector('.imagem')

function previous () {
    if (activeImage < images.length - 1) {
        activeImage += 1
    } else {
        activeImage = 0
    }

    mainImage.src = images[activeImage]
}

function next () {
    if (activeImage > 0) {
        activeImage -= 1
    }else {
        activeImage = images.length - 1;
    }
    mainImage.src = images[activeImage]
}

buttonNext.addEventListener('click', previous)

buttonPrevious.addEventListener('click', next)

document.addEventListener('keydown', function (event) {
    if (event.keyCode === 37) {
        next()
    }

    if (event.keyCode === 39) {
        previous()
    }
})


var fieldInputs = document.querySelectorAll('.field-input input')

for (var i = 0; i < fieldInputs.length; i++) {
    fieldInputs[i].addEventListener('input', function(e) {
        if (e.target.value) {
            e.target.classList.add('-value')
        } else {
            e.target.classList.remove('-value')
        }

    })
}